import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PostService } from '../shared/post.service';
import { Post } from '../shared/post.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit, OnDestroy {
  @ViewChild('f') postForm!: NgForm;

  isUploading = false;
  postUploadingSubscription!: Subscription;

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postUploadingSubscription = this.postService.postUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  savePost() {

    const id = Math.random().toString();

    const post = new Post(
      id,
      this.postForm.value.author,
      this.postForm.value.message,
      this.postForm.value.datetime
    );

    this.postService.addPost(post).subscribe(() => {
      this.postService.fetchPosts();
    });
  }

  ngOnDestroy() {
    this.postUploadingSubscription.unsubscribe();
  }
}
