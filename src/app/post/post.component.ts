import { Component, OnDestroy, OnInit } from '@angular/core';
import { Post } from '../shared/post.model';
import { PostService } from '../shared/post.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit, OnDestroy {

  posts: Post[] = [];
  postsChangeSubscription!: Subscription;
  postsFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.postsChangeSubscription = this.postService.postsChange.subscribe((posts: Post[]) => {
      this.posts = posts;
    });
    this.postsFetchingSubscription = this.postService.postsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

    this.postService.fetchPosts();

    setInterval(() => {
      this.postService.fetchPosts();
    }, 15000);
  }

  ngOnDestroy() {
    this.postsChangeSubscription.unsubscribe();
    this.postsFetchingSubscription.unsubscribe();
  }

}
