export class Post {
  constructor(
    public _id: string,
    public author: string,
    public message: string,
    public datetime: string,
  ) {}
}
