import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()

export class PostService {
  postsChange = new Subject<Post[]>();
  postsFetching = new Subject<boolean>();
  postUploading = new Subject<boolean>();

  private posts: Post[] = [];

  constructor(private http: HttpClient) {}

  fetchPosts() {
    this.postsFetching.next(true);
    return this.http.get<{[key: string]: Post}>('http://146.185.154.90:8000/messages')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const postData = result[id];
          return new Post(id, postData.author, postData.message, postData.datetime);
        });
      }))
      .subscribe(posts => {
        this.posts = posts.reverse();
        this.postsChange.next(this.posts.slice());
        this.postsFetching.next(false);
      }, () => {
        this.postsFetching.next(false);
      });
  }

  addPost(post: Post) {
    const body = new HttpParams()
      .set('author', post.author)
      .set('message', post.message);

    this.postUploading.next(true);

    return this.http.post('http://146.185.154.90:8000/messages', body).pipe(
      tap(() => {
        this.postUploading.next(false);
      }, () => {
        this.postUploading.next(false);
      })
    );
  }
}
